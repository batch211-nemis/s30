db.fruits.insertMany([
{
name : "Apple",
color : "Red",
stock : 20,
price: 40,
supplier_id : 1,
onSale : true,
origin: [ "Philippines", "US" ]
},
{
name : "Banana",
color : "Yellow",
stock : 15,
price: 20,
supplier_id : 2,
onSale : true,
origin: [ "Philippines", "Ecuador" ]
},
{
name : "Kiwi",
color : "Green",
stock : 25,
price: 50,
supplier_id : 1,
onSale : true,
origin: [ "US", "China" ]
},
{
name : "Mango",
color : "Yellow",
stock : 10,
price: 120,
supplier_id : 2,
onSale : false,
origin: [ "Philippines", "India" ]
}
]);

/////////////////////////////////////////

//MONGODB AGGREGATION
/*
	- used to generate manipulated data and perform operations to create filtered results that helps in analytics data.
	- compared to doing cRUD Operations in our data from our previous sessions, aggregations gives us access to manipulate, filter, and compute for results, providing us with information to make necessary development decisions.
	- aggregates in MongoDB are very flexible and you can form your own aggregation pipeline depending on the need of your application/
*/

//AGGREGATE METHODS

/*
	1. MATCH: "$match"
		- used to pass the documents that meet the specifies condition(s) to the next pipeline stage/aggregation process.

		Syntax:
			[$match: {field: value}]
*/

db.fruits.aggregate([
	{$match: {onSale: true}}
]);

db.fruits.aggregate([
	{$match: {stock: {$gte:20}}}
]);

/*
	2. GROUP: "$group"
		- used to group elements together field-value pairs using teh data from the grouped element.

		Syntax:
			{$group: {_id: "value", fieldResult: "valueResult"}}
*/

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", total: { $sum: "$stock"}}}
]);

/*
	3. SORT: "$sort"
		- can be used when changing the order of the aggregated results.
		- providing a value of negative (-1) will sort the documnets in a descending order.
		- providing value of 1 will sort the documents in an ascending order.

		Syntax:
			{$sort { field: 1/-1}}
*/

db.fruits.aggregate([
	{$match: { onSale: true} },
	{$group: { _id: "$supplier_id", total: {$sum: "$stock"} } },
	{$sort: {total: -1} }
]);

/*
	4. PROJECT: "$project"
		- can be used when aggregating data to include or exclude from the returned results.

		Syntax:
			{$project: {field: 1/0}}
*/

db.fruits.aggregate([
	{$match: { onSale: true} },
	{$group: { _id: "$supplier_id", total: {$sum: "$stock"} } },
	{$project: {_id: 0} }
]);

db.fruits.aggregate([
	{$match: { onSale: true} },
	{$group: { _id: "$supplier_id", total: {$sum: "$stock"} } },
	{$project: {_id: 0} },
	{$sort: {total: -1} }
]);

/* AGGREGATION PIPELINES
		- The aggregation pipeline in MongoDB is a framework for data aggregation.
		- Each stage transforms the documents as they pass through the deadlines.

		Syntax:
			db.collectionName.aggregate([
				{stageA},
				{stageB},
				{stageC}
			])
*/

//OPERATORS

// #1 "$sum" - gets the total of everything

db.fruits.aggregate([
	{$group: { _id: "$supplier_id", total: {$sum: "$stock"} } },
]);

// #2 "$max" - gets the highest value out of everything else.

db.fruits.aggregate([
	{$group: { _id: "$supplier_id", max_price: {$max: "$price"} } }
]);

// #3 "$min" - gets the lowest value out of everything else.

db.fruits.aggregate([
	{$group: { _id: "$supplier_id", min_price: {$min: "$price"} } }
]);

// #4: "$avg" - gets the average value of all the fields.

db.fruits.aggregate([
	{$group: { _id: "$supplier_id", avg_price: {$avg: "$price"} } }
]);